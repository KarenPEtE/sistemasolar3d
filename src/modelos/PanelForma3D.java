package modelos;

import javax.media.j3d.*;

public class PanelForma3D extends Panel3D {
  
  private final Node node;

  public PanelForma3D(Node node) {
    this.node = node;
    addUniverso();
    setOrbitBehavior(true);
  }
  
  @Override
  public Node addNodoPrincipal() {
    BranchGroup bg = new BranchGroup();
    bg.addChild(node);
    return bg;
  }
}