package modelos;

import java.awt.Color;
import javax.media.j3d.*;
import javax.vecmath.*;

public class Luces {
  public static final Bounds BOUNDS = new BoundingSphere(new Point3d(0, 0, 0), 4);
  
  public static AmbientLight addLuzAmbiental(Color color) {
    AmbientLight ambiental = new AmbientLight(
      new Color3f(color)
    );
    ambiental.setInfluencingBounds(BOUNDS);
    return ambiental;
  }
  
  public static final DirectionalLight addLuzDireccional(Color color, Vector3f direccion) {
    DirectionalLight direccional = new DirectionalLight(
      new Color3f(color),
      direccion
    );
    direccional.setInfluencingBounds(BOUNDS);
    return direccional;
  }
  
  public static final PointLight addLuzPuntual(Color color, Point3f ubicacion, Point3f atenuacion) {
    PointLight puntual = new PointLight(
      new Color3f(color),
      ubicacion,
      atenuacion
    );
    puntual.setInfluencingBounds(BOUNDS);
    return puntual;
  }
  
  public static final SpotLight addLuzSpot(Color color, Point3f ubicacion, Point3f atenuacion, Vector3f direccion, float angulo, float concentracion) {
    SpotLight spot = new SpotLight(
      new Color3f(color),
      ubicacion,
      atenuacion,
      direccion,
      angulo,
      concentracion
    );
    spot.setInfluencingBounds(BOUNDS);
    return spot;
  }

  public static Light luzDireccional(Color c, Vector3f v) {
    DirectionalLight l = new DirectionalLight(new Color3f(c), v);
    l.setInfluencingBounds(new BoundingSphere());
    return l;

  }

  public static Light luzAmbiental(Color c) {
    AmbientLight l = new AmbientLight(new Color3f(c));
    l.setInfluencingBounds(new BoundingSphere());
    return l;
  }

  public static Light luzPuntual(Color c, Point3f p, Point3f at) {
    PointLight l = new PointLight(new Color3f(c), p, at);
    l.setInfluencingBounds(new BoundingSphere());
    return l;
  }

  public static Light luzSpot(Color c, Point3f p, Point3f at,
    Vector3f dir, float ang, float con) {
    SpotLight l = new SpotLight(new Color3f(c), p, at, dir, ang, con);
    l.setInfluencingBounds(new BoundingSphere());
    return l;
  }
}
