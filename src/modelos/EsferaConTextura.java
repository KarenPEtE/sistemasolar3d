package modelos;

import com.sun.j3d.utils.geometry.Sphere;
import javax.media.j3d.Material;

public class EsferaConTextura extends Sphere {

  public EsferaConTextura(float radio, String archivo) {
    super(radio, GENERATE_TEXTURE_COORDS | GENERATE_NORMALS, 150);
    cargarTextura(archivo);
  }

  public final void cargarTextura(String archivo) {
    Apariencia ap = new Apariencia();
    ap.asignarTextura(archivo);
    ap.setMaterial(new Material());
    setAppearance(ap);
  }
}
