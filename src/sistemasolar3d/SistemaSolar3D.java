package sistemasolar3d;

import javax.swing.JFrame;


public class SistemaSolar3D {

  public static void main(String[] args) {
    SistemaSolar sistema = new SistemaSolar();                          //MODELO
    PanelSistemaSolar panel = new PanelSistemaSolar(sistema);           //VISTA
    OyenteSistemaSolar oyente = new OyenteSistemaSolar(sistema, panel); //CONTROLADOR
    panel.addEventos(oyente);                                           //REGISTRO
    JFrame f = new JFrame("Sistema Solar Tridimensional");
    f.setSize(800, 600);
    f.setLocation(50, 50);
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    f.add(panel);
    f.setVisible(true);
  }
}
