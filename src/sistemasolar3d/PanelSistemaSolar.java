package sistemasolar3d;

import java.awt.BorderLayout;
import javax.swing.*;
import modelos.PanelForma3D;


public class PanelSistemaSolar extends JPanel {

  private JButton botonIniciar;
  private final SistemaSolar sistema;

  public PanelSistemaSolar(SistemaSolar sistema) {
    this.sistema = sistema;
    addComponentes();
  }

  public void addEventos(OyenteSistemaSolar oyente) {
    botonIniciar.addActionListener(oyente);
  }

  private void addComponentes() {
    JPanel panelNorte = new JPanel();
    this.setLayout(new BorderLayout());
    botonIniciar = new JButton("Iniciar Movimiento");
    panelNorte.add(botonIniciar);
    add(panelNorte, "North");
    add(new PanelForma3D(sistema),"Center");
  }
  
}